import re
import math
import pandas as pd
import spacy
import DataFrameBuilder
class LoadIndex:
    
    
    
    """ we define words which should not be lemmatized in the instance variable not_to_be_lemmatized
        the data is read from the csv and a list(variable named filenames) consisting of name(index) is created
    """
    def __init__(self, fileDir,db_dataframe,counter):
        self.tf = {}
        self.df = {}
        self.idf = {}
        self.not_to_be_lemmatized=['windows']                   # list of words not to be lemmatized
        self.dataFrame=DataFrameBuilder.caller(fileDir,db_dataframe,counter) #name,link,title,content
        d = DataFrameBuilder.databasecaller()
        d.tagBuilder()
        if (type(self.dataFrame)==bool):
            print('x')
        else:
            print('y')
        self.nlp=spacy.load("en_core_web_sm")
        self.filenames=self.dataFrame['name'].tolist()
        # below this line, functions are called; the explanations of which are written before their definitions
        self.file_to_terms = self.process_files()
        self.regdex = self.make_indices(self.file_to_terms)
        self.totalIndex = self.full_index()
        self.vectors = self.vectorize()
        self.mags = self.calc_magnitudes(self.filenames)
        self.populate_scores()

    """ the list (filenames) created in the __init__ function is used for looping through every element(index) of the csv file
        And the corresponding content (page content from csv) is stored inside file_to_terms dictionary
        #Note: the content is converted to lowercase
        
        Now comes the main part, i.e. creation of n-grams
        the threshold(maximum number of words in a phrase) is kept as 3 which means phrases of length 1,2 and 3 would be created 
        
        An example of n-grams:
            Given a string "Microsoft office locations"
            the n-grams for the above string would look like
            ['microsoft','office','location','microsoft office','office location','microsoft office location'] #The threshold is set to 3
        
        the words are then processed by the stopLemmatizer function, the description of which is given later.
    """
    def process_files(self):
        file_to_terms = {}
        for file in self.filenames:
            pattern = re.compile('[\W_]+')
            file_to_terms[file]=self.dataFrame['title'][file-1].lower()+" "+self.dataFrame['content'][file-1].lower()  #file-1 because file no starts from 1 and index from 0
            file_to_terms[file] = pattern.sub(' ',file_to_terms[file])
            re.sub(r'[\W_]+','', file_to_terms[file])
            splitted_terms=file_to_terms[file].split()
            # creation of n-grams ( the core part of this function )
            ngrams=[]
            for i in range(0,len(splitted_terms)-2):
                ngrams.append(splitted_terms[i]+" "+splitted_terms[i+1])
                ngrams.append(splitted_terms[i+1]+" "+splitted_terms[i+2])
            file_to_terms[file]=self.stopLemmatizer(file_to_terms[file])        # for removal of stopwords and lemmatization(wherever possible) 
            file_to_terms[file]+=ngrams
            file_to_terms[file]+=[self.dataFrame['title'][file-1].lower().split('.')[0]]
        return file_to_terms
    
    
    
    
    """ Using spacy , stopwords are removed
        Further, the words are lemmatized 
    """
    def stopLemmatizer(self,file_terms):
        stemmed=[]
        doc=self.nlp(file_terms)
        self.nlp.vocab["that"].is_stop=True                 # 'that' should be a stopword , but spacy does not consider it by default 
        self.nlp.vocab["one"].is_stop=False                 # 'one' is considered as a stopword, but we have keywords having the word 'one' in them e.g. XBox One, One Drive
        stopped = [w for w in doc if w.is_stop is False]    #w not in stopwords
        for term in stopped:
            if term.text in self.not_to_be_lemmatized:      # don't lemmatize
                stemmed.append(term.text)                   
            else:                                           # lemmatize
                stemmed.append(term.lemma_)                 
        return stemmed




    """
        This function creates a dictonary of words for a file
        the dictionary created contains unique words from the file as keys 
        and the values are nothing but arrays containing positions of that word in the file
        input  = [word1, word2, ...]
        output = {word1: [pos1, pos2], word2: [pos2, pos434], ...}
    """
    def index_one_file(self, termlist):
        fileIndex = {}
        for index, word in enumerate(termlist):
            if word in fileIndex.keys():                        # if the word was previously found, then just append the new position
                fileIndex[word].append(index)
            else:                                               # since this is a new word, we need to create a new entry
                fileIndex[word] = [index]
        return fileIndex
    
    
    

    """ this function calls the above index_one_file function for every entry(row) in thr csv i.e for every page
    	input  = {filename: [word1, word2, ...], ...}
        output = {filename: {word1: [pos1, pos2, ...], ...}, ...}
    """
    def make_indices(self, termlists):
        total = {}
        for filename in termlists.keys():               
            total[filename] = self.index_one_file(termlists[filename])
        return total



    """ Gist: Calculation of term frequency and document frequency takes place here ( TF-IDF )
    
        Term Frequency: The number of times a word is repeated in a **single** file is the term frequency of that word in the corresponding file
        (The term frequency is calculated as the length of the list (which contained positions of word in a file))
        Document Frequency: The number of documents containing a word is document frequency of a word
        
    	input = {filename: {word: [pos1, pos2, ...], ... }}
        output = {word: {filename: [pos1, pos2]}, ...}, ...}    
    """
    def full_index(self):
        total_index = {}
        indie_indices = self.regdex                     # just for convenience 
        for filename in indie_indices.keys():
            self.tf[filename] = {}
            for word in indie_indices[filename].keys():
                self.tf[filename][word] = len(indie_indices[filename][word])        # here length gives the frequency of that particular word in a given file - Term Frequency
                if word in self.df.keys():                                          # if a word was present previously in other file, then no need to create a new entry (just increase the count) - Document Frequency
                    self.df[word] += 1
                else:                                                               # else create a new entry
                    self.df[word] = 1 
                # the below part is for storing the names of files and positions of that word corresponding every key(word)
                # the following code makes the variable total_index look like {word: {filename: [pos1, pos2]}, ...}, ...}    
                if word in total_index.keys():
                    if filename in total_index[word].keys():
                        total_index[word][filename].append(indie_indices[filename][word][:])    #only append the position as filename and word both are already present
                    else:
                        total_index[word][filename] = indie_indices[filename][word]             #create an entry for file ( note that word is already present)
                else:
                    total_index[word] = {filename: indie_indices[filename][word]}               #create a completely new entry as a new word is encountered
        return total_index



    """ More the characters matching, more is the weightage .... keeping this thing in mind, we have created vectors
        wherein the length of the word is used to denote its weightage in the file vector
        input = {filename: [word1, word2, ....], ...}
        output= {filename: [len1, len2, ...], ...}
    """
    def vectorize(self):
        vectors = {}
        for filename in self.filenames:
            vectors[filename] = [len(self.regdex[filename][word]) for word in self.regdex[filename].keys()]
        return vectors



    """ A simple function which returns the number of rows ( simply the number of files )
    """
    def collection_size(self):
        return len(self.filenames)



    """ The magnitude of the file vector is calculated 
        Magnitude in our case is nothing but the
        square root of ( sum of ( squares of all elements ) )
        input = {filename: [len1, len2, ...], ...}
        output= {filename: val1, .....}
    """
    def calc_magnitudes(self, documents):
        mags = {}
        for document in documents:
            mags[document] = pow(sum(map(lambda x: x**2, self.vectors[document])),.5)
        return mags



    """ Given a term and the document,
        this function calculates the weight of that term in the document and returns the same
        if a term is present in a file, then it returns the calculated value 
        else the term frequency returned for the document is zero
    """
    def term_frequency(self, term, document):
        return self.tf[document][term]/self.mags[document] if term in self.tf[document].keys() else 0



    """ now , till this point we have our file vectors (magnitude) ready and term frequency(total), doc. freq ready
        the thing required is term frequency(in a document) and **inverse** document frequency -- main part of this function 
    """
    def populate_scores(self): 
        for filename in self.filenames:
            for term in self.getUniques():
                self.tf[filename][term] = self.term_frequency(term, filename)
                if term in self.df.keys():
                    self.idf[term] = self.idf_func(self.collection_size(), self.df[term]) 
                else:
                    self.idf[term] = 0
        return self.df, self.tf, self.idf,self.dataFrame


    """ Given a doucment fequency (N)
        and number of documents (N_t)
        this function calculates the Inverse Document Frequency and returns the same.
    """
    def idf_func(self, N, N_t):
        if N_t != 0:
            return math.log(N/N_t)
        else:
            return 0



    """ The TF-IDF is calculated by multiplying the term frequncy and inverse document frequency
        This function does the same thing and returns the value obtained on multiplying 
        term frequency with the inverse document frequency.
    """
    def generateScore(self, term, document):
        return self.tf[document][term] * self.idf[term]



    """ A simple function to return unique values from totalIndex
    """
    def getUniques(self):
        return self.totalIndex.keys()
