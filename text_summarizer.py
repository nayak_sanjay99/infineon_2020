import numpy as np
import pandas as pd
import pickle
from tensorflow.keras.models import load_model
from keras.preprocessing.sequence import pad_sequences
encoder_model = load_model('encoder.h5',compile=False)
decoder_model = load_model('decoder.h5',compile=False)

def decode_sequence(text):
    with open('file3.pkl', 'rb') as handle:
        x_tokenizer = pickle.load(handle)
    with open('file1.pkl', 'rb') as handle:
        target_word_index = pickle.load(handle)    
    with open('file2.pkl', 'rb') as handle:
        reverse_target_word_index = pickle.load(handle)
    max_text_len=100
    post_data2=pd.DataFrame({'text':[text]})
    x_2 = np.array(post_data2['text'])
    x_3 = x_tokenizer.texts_to_sequences(x_2)
    x_3 = pad_sequences(x_3,maxlen=max_text_len,padding='post')
    input_seq = x_3.reshape(1,max_text_len)

    max_summary_len=15

    # Encode the input as state vectors.
    e_out, e_h, e_c = encoder_model.predict(input_seq)
    
    # Generate empty target sequence of length 1.
    target_seq = np.zeros((1,1))

    # Populate the first word of target sequence with the start word.
    target_seq[0, 0] = target_word_index['sostok']


    stop_condition = False
    decoded_sentence = ''
    while not stop_condition:
      
        output_tokens, h, c = decoder_model.predict([target_seq] + [e_out, e_h, e_c])
        # Sample a token
        sampled_token_index = np.argmax(output_tokens[0, -1, :])
        sampled_token = reverse_target_word_index[sampled_token_index]
        
        if(sampled_token!='eostok'):
            decoded_sentence += ' '+sampled_token

        # Exit condition: either hit max length or find stop word.
        if (sampled_token == 'eostok'  or len(decoded_sentence.split()) >= (max_summary_len-1)):
            stop_condition = True

        # Update the target sequence (of length 1).
        target_seq = np.zeros((1,1))
        target_seq[0, 0] = sampled_token_index

        # Update internal states
        e_h, e_c = h, c

    return decoded_sentence
