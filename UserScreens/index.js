window.onload=function(){
    // document.getElementById('synUpdater').style.transform="scale(1,1)"
    // document.getElementById('synUpdater').style.transition="transform 1ms cubic-bezier(0.4, 0, 0.2, 1) 200ms;"
    fetch(`http://localhost:5000/tag_dict`).then(res=>{
        res.json().then(res=>{
            console.log(res)
            this.localStorage.setItem('keywords',JSON.stringify(res[0]))
            this.localStorage.setItem('values',JSON.stringify(res[1]))
            for(i=0;i<res[0].length;i++)
                document.getElementById('search_categories').innerHTML+=`<option value=${i}>${res[0][i]}</option>`
        })
    })
    setTimeout(() => {
        document.getElementById('syn_updater_2').style.display='block';
        setTimeout(() => {
            document.getElementById('syn_updater_3').style.display='block';
            setTimeout(() => {
                document.getElementById('reRunner').style.display='flex';
            }, 50);
        }, 50);
    }, 50);
}


document.getElementById('search_categories').onchange=function(e){
    var syns=JSON.parse(localStorage.getItem('values'))
    console.log(syns)
    console.log(e.target.value)
    document.getElementById('syn_list').value=syns[e.target.value]
}

document.getElementById('syn_updater').onsubmit=function(e){
    e.preventDefault()
    var val=document.getElementById('search_categories').value
    if(val!="--"){
        var keywords=JSON.parse(localStorage.getItem('keywords'))
        var keyword=keywords[Number.parseInt(val)]
        var synonyms=document.getElementById('syn_list').value
        fetch(`http://localhost:5000/tag_adder?title=${keyword}&tags=${synonyms}`).then(res=>{
            res.json().then(res=>{
                console.log(res)
                alert(res.status)
            })
        })
    }
}

document.getElementById('re_run').onclick=function(){
    window.location.href='../index.html'
}