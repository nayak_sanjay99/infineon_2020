import loadindex
import re
import pandas
import spacy


class Query:
    

    def __init__(self,fileDir,db_dataframe,counter):
        # first we load the dictionary and necesary variables from loadIndex file
        self.index = loadindex.LoadIndex(fileDir,db_dataframe,counter)
        self.invertedIndex = self.index.totalIndex
        self.regularIndex = self.index.regdex
        self.strng=[]
        self.cStrng=[]
        
        #loading spacy and required csv
        self.nlp=spacy.load('en_core_web_sm')
        self.pd_syn=pandas.read_csv('./synonyms.csv',sep=",")
        self.keys=self.pd_syn['Keywords'].tolist()
        self.not_to_be_lemmatized=['windows']                          #as 'windows' is a keyword (window<-windows)


    """ given a word/n-gram, it is searched inside all file, (with the help of previously
        created dictionary - from loadIndex file ) and then based on 
        score (cosine similarity between word vector and file vector), the results
        are ranked.
    """
    def one_word_query(self, word):
        pattern = re.compile('[\W_]+')
        word = pattern.sub(' ',word)
        #print(word)
        if word in self.invertedIndex.keys():        # if word is found in any of the files
            return self.rankResults([filename for filename in self.invertedIndex[word].keys()], word,0)
        else:                                        # if word is not found in any of the files
            return []
        

    """ given a query string(list of words),
        the obove one_word_query function is called for every word
        following which the result set is ranked
    """
    def free_text_query(self, stemmed, strng):
        result = []
        for word in stemmed:
            result += self.one_word_query(word)
        return self.rankResults(list(set(result)), strng, 1)
    

    """ Given a file, based on the number of words/n-grams from the file that has matched
        and the score obtained from each matching, a vector is created for 
        the corresponding file(document)    
    """
    def make_vectors(self, documents):
        vecs = {}
        for doc in documents:
            docVec = [0]*len(self.index.getUniques())
            for ind, term in enumerate(self.index.getUniques()):
                docVec[ind] = self.index.generateScore(term, doc)
            vecs[doc] = docVec
        return vecs


    """ Now that we have scores for individual words/n-grams
        we can form a TF-IDF vector for the whole query.
        This process is pretty much the same as done for creating TF-IDF vector for a file 
        in the loadIndex.py file.
    """
    def query_vec(self, query):
        pattern = re.compile('[\W_]+')
        query = pattern.sub(' ',query)
        queryls = query.split()
        queryVec = [0]*len(queryls)
        index = 0
        for ind, word in enumerate(queryls):
            queryVec[index] = self.queryFreq(word, query)
            index += 1
        queryidf = [self.index.idf[word] for word in self.index.getUniques()]
        magnitude = pow(sum(map(lambda x: x**2, queryVec)),.5)    # calulating the sqrt( sum of squares )
        freq = self.termfreq(self.index.getUniques(), query)
        tf = [x/magnitude for x in freq]                          # normalization ny dividing the vector by its magnitude 
        final = [tf[i]*queryidf[i] for i in range(len(self.index.getUniques()))]
        return final
    

    """  a simple function to calculate the frequency of a term in the query itself
    """
    def queryFreq(self, term, query):
        count = 0
        for word in query.split():
            if word == term:
                count += 1
        return count
    

    """ this function calculates the frequency of every unique word in the user query
        the above queryFreq function is called for every "unique term"
    """
    def termfreq(self, terms, query):
        temp = [0]*len(terms)
        for i,term in enumerate(terms):
            temp[i] = self.queryFreq(term, query)
            #print(self.queryFreq(term, query))
        return temp
    

    """ a simple function to calculate the dot product of two vectors 
        - this is used during cosine similarity calculation
    """
    def dotProduct(self, doc1, doc2):
        if len(doc1) != len(doc2):
            return 0
        return sum([x*y for x,y in zip(doc1, doc2)])
    

    """this function sorts the results based on the credits (cosine similarity) """
    def rankResults(self, resultDocs, query, fin):
        
        # the fin variable is set when this rankResults function is called for the last time 
        
        vectors = self.make_vectors(resultDocs)
        queryVec = self.query_vec(query)
        results = [[self.dotProduct(vectors[result], queryVec), result] for result in resultDocs]
        results.sort(key=lambda x: x[0],reverse=True)  #sorting the results in descending order
        results = [x[1] for x in results]              # only keeping the index value
        #if fin is 1:
            #for elm in results:
                #print(str(elm)+"\t"+self.index.dataFrame['link'][elm-1])
        return results
    
    
    """ creation of n-grams (threshold is set to 3)"""
    def n_grammer(self,strng):
        grams=[]
        words=strng.split()
        if(len(words) is 1):                # no n-grams required
            return grams
        if(len(words) is 2):                #the only possible n-gram is the string itself
            return [words[0]+" "+words[1]]
        #for i in range(0,len(words)-2):     # formation of n-grams( n<=3 )
        #    grams.append(words[i]+" "+words[i+1]+" "+words[i+2])
        #    grams.append(words[i]+" "+words[i+1])
        #    grams.append(words[i+1]+" "+words[i+2])
        return grams
    
    

    
    """this function returns a list of synonyms based on user's query"""
    def syn_finder(self,stemmed):
        syn_arr=[]
        for elm in stemmed:
            if elm in self.keys:
#                 print(elm)
                syn_arr+=self.pd_syn['Synonyms'][self.keys.index(elm)].split(',')
        return syn_arr
    
 
    """basic string manipulation, removal of stopwords, addition of synonyms is done in this function."""
    def query_processor(self,strng,scp):
       #inOut is a functin from spellCheckPack file responsilbe for spelling-corrections, if any
        self.cStrng=scp.inOut(strng)
        print(self.cStrng)
        self.strng=strng
        strng=strng.lower()
        #print(strng)
        pattern = re.compile('[\W_]+')
        string = pattern.sub(' ',strng)
        n_grams=self.n_grammer(strng)                          #creating n-grams
        stemmed=[]
        doc=self.nlp(string)                                    #creating spacy-NLP object
        self.nlp.vocab["that"].is_stop=True                     #'that' isn't a keyword by default in spacy
        self.nlp.vocab["one"].is_stop=False                     #we have keywords like laptop models having keyword 'one'
        
        #stopwords removal
        stopped = [w for w in doc if w.is_stop is False]        
        
        #lemmatization(wherever necessary)
        for token in stopped:
            if token.text in self.not_to_be_lemmatized:
                stemmed.append(token.text)
            else:
                stemmed.append(token.lemma_)
                
        #adding n_grams
        stemmed+=n_grams
        
        #adding synonyms
        stemmed+=self.syn_finder(stemmed)  
        
        return self.free_text_query(stemmed,strng),self.cStrng
    
