import os
import sys
import pandas as pd
import slate3k as slate
import docx
from pptx import Presentation
import zipfile
import xml.dom.minidom
import pickle
import tagging
import text_summarizer 
import xlsx_csv_work

class DataFrameBuilder:
    def __init__(self):
        self.pickleData=None
        if os.path.exists('mypickle.pickle') is True:
            self.pickleData=pd.read_pickle('mypickle.pickle')
        pass

    
    def dataFrameBuilder(self,fileDir,db_dataframe,counter): 
        folders = fileDir.split(';;;')
        data=db_dataframe
        repeat_policer=[]
        if(self.pickleData is None):
            for folder in folders:
                for x in os.walk(folder):
                    for file in x[2]:
                        typeOfFile=file.split('.')[-1]
                        print(typeOfFile)
                        filepath =x[0]+'/'+file
                        filepath=filepath.replace('\\','/')
                        print(repeat_policer)
                        print(filepath)
                        if filepath not in repeat_policer:
                            repeat_policer.append(filepath)
                            if(typeOfFile == "xlsx" or typeOfFile == "xls"):
                                print(filepath)
                                excel_data = pd.ExcelFile(filepath)
                                sheet_names = excel_data.sheet_names
                                for sheet in sheet_names:
                                    temp = []
                                    temp.append(counter)
                                    counter = counter+1
                                    temp.append(typeOfFile)
                                    temp.append(filepath)
                                    temp.append(file) 
                                    df = pd.read_excel(excel_data,sheet)
                                    content = xlsx_csv_work.content(df)
                                    temp.append(content)
                                    temp.append(xlsx_csv_work.tags(df))
                                    summ = "start An excel sheet containing data:" + content + ". end"
                                    temp.append(summ)
                                    data.append(temp)
                            elif(typeOfFile == "csv"):
                                    temp = []
                                    temp.append(counter)
                                    counter = counter+1
                                    temp.append(typeOfFile)
                                    temp.append(filepath)
                                    temp.append(file) 
                                    df = pd.read_csv(filepath)
                                    content = xlsx_csv_work.content(df)
                                    temp.append(content)
                                    temp.append(xlsx_csv_work.tags(df))
                                    summ = "start An csv containing data:" + content + ". end"
                                    temp.append(summ)
                                    data.append(temp)                       
                            else:
                                temp=[]
                                temp.append(counter)
                                counter=counter+1
                                temp.append(typeOfFile)
                                temp.append(filepath)
                                temp.append(file)
                                if(typeOfFile=='txt'):
                                    f = open(filepath, 'r')
                                    temp.append(f.read())
                                    f.close()
                                elif(typeOfFile=='pdf'):
                                    temp.append(self.pdfextract(filepath))
                                elif(typeOfFile=='docx'):
                                    temp.append(self.docxextract(filepath))
                                elif(typeOfFile=='pptx'):
                                    temp.append(self.pptxextract(filepath))
                                elif(typeOfFile=='odt'):
                                    temp.append(self.odtextract(filepath))
                                tags = tagging.main(temp[4])
                                temp_tags = ""
                                for tag in tags:
                                    if(temp_tags != ""):
                                        temp_tags += ","       
                                    temp_tags+=tag
                                temp.append(temp_tags)
                                summary = text_summarizer.decode_sequence(temp[4])
                                temp.append(summary)
                                data.append(temp)
            self.pickleData=pd.DataFrame(columns=["name", "type","link","title","content","tags","summary"], data=data)
        else:
            print('inside pickle')
            sites=list(self.pickleData['link'])
            counter = len(sites) + 1
            for folder in folders:
                for x in os.walk(folder):
                    for file in x[2]:
                        tuplex=x[0].replace('\\','/')
                        if(tuplex+'/'+ file not in sites):
                            typeOfFile=file.split('.')[-1]
                            filepath = tuplex+'/'+ file
                            if(typeOfFile=="xlsx" or typeOfFile == "xls"):
                                print(filepath)
                                excel_data = pd.ExcelFile(filepath)
                                sheet_names = excel_data.sheet_names
                                for sheet in sheet_names:
                                    temp = []
                                    temp.append(counter)
                                    counter = counter+1
                                    temp.append(typeOfFile)
                                    temp.append(filepath)
                                    temp.append(file) 
                                    df = pd.read_excel(excel_data,sheet)
                                    content = xlsx_csv_work.content(df)
                                    temp.append(content)
                                    temp.append(xlsx_csv_work.tags(df))
                                    summ = "start An excel sheet containing data:" + content + ". end"
                                    temp.append(summ)
                                    self.pickleData.loc[len(self.pickleData)] = temp
                            elif(typeOfFile=="csv"):
                                temp = []
                                temp.append(counter)
                                counter = counter+1
                                temp.append(typeOfFile)
                                temp.append(filepath)
                                temp.append(file) 
                                df = pd.read_csv(filepath)
                                content = xlsx_csv_work.content(df)
                                temp.append(content)
                                temp.append(xlsx_csv_work.tags(df))
                                summ = "start An csv containing data:" + content + ". end"
                                temp.append(summ)
                                self.pickleData.loc[len(self.pickleData)] = temp
                            else:
                                temp=[]
                                temp.append(counter)
                                counter=counter+1
                                temp.append(typeOfFile)
                                temp.append(filepath)
                                temp.append(file)
                                if(typeOfFile=='txt'):
                                    f = open(filepath, 'r')
                                    temp.append(f.read())
                                    f.close()
                                elif(typeOfFile=='pdf'):
                                    temp.append(self.pdfextract(filepath))
                                elif(typeOfFile=='docx'):
                                    temp.append(self.docxextract(filepath))
                                elif(typeOfFile=='pptx'):
                                    temp.append(self.pptxextract(filepath))
                                elif(typeOfFile=='odt'):
                                    temp.append(self.odtextract(filepath))
                                tags = tagging.main(temp[4])
                                temp_tags = ""
                                for tag in tags:
                                    if(temp_tags != ""):
                                        temp_tags += ","    
                                temp_tags+=tag
                                temp.append(temp_tags)
                                summary = text_summarizer.decode_sequence(temp[4])
                                temp.append(summary)    
                                self.pickleData.loc[len(self.pickleData)] = temp
        print(self.pickleData)
        self.pickleData.to_pickle('mypickle.pickle')
        return self.pickleData
    
    def tagBuilder(self):
        self.tagsData=None
        if os.path.exists('Tags.csv') is True:
            self.tagsData=pd.read_csv('Tags.csv')
        if(self.tagsData is None):
            self.tagsData = pd.DataFrame({"title": self.pickleData["title"],"tags":self.pickleData["tags"]})
            self.tagsData.to_csv(path_or_buf='./Tags.csv',index=False)
        else:
            title_pickle=list(self.pickleData['title'])
            title_tagsData=list(self.tagsData['title'])
            for title in title_pickle:
                if title not in title_tagsData:
                    temp=[]
                    temp.append(title)
                    tagsval = self.pickleData[self.pickleData["title"]==title]["tags"]
                    tagsval = tagsval.tolist()
                    temp.append(tagsval[0])
                    self.tagsData.loc[len(self.tagsData)] = temp
                    self.tagsData.to_csv(path_or_buf='./Tags.csv',index=False)
        pass        

    def pdfextract(self,file):
        with open(file, 'rb') as f:
            fileReader = slate.PDF(f)
            text = []
            for doc in fileReader:
                text.append(doc.lower().replace("\n"," ").replace("  "," "))
        return text[0]
    
    def docxextract(self,filename):
        doc = docx.Document(filename)
        fullText = []
        for para in doc.paragraphs:
            fullText.append(para.text)
        return ' '.join(fullText)
    
    def pptxextract(self,filename):
        prs = Presentation(filename)
        text=[]
        for slide in prs.slides:
            for shape in slide.shapes:
                text.append(shape.text)
        return ' '.join(text).replace('\n',' ')

    def odtextract(self,filepath):
        myfile = zipfile.ZipFile(filepath)
        filelist = myfile.infolist()
        ostr = myfile.read('content.xml')
        doc = xml.dom.minidom.parseString(ostr)
        paras = doc.getElementsByTagName('text:p')
        text_in_paras = []
        content = ""
        for p in paras:
            for ch in p.childNodes:
                if ch.nodeType == ch.TEXT_NODE:
                    text_in_paras.append(ch.data)
        for s in text_in_paras:
            content += s
        return content

def caller(fileDir,db_dataframe,counter):
    d=DataFrameBuilder()
    return d.dataFrameBuilder(fileDir,db_dataframe,counter)

def databasecaller():
    d = DataFrameBuilder()
    return d
