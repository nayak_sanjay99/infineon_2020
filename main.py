import searchRanker
import file_adder_sql
import file_retreiver_sql
import file_adder_mongo
import file_retreiver_mongo
import subprocess,os,platform
import tag_dict
import tag_adder
import tag_search
import tkinter as Tkinter
from tkinter import filedialog as tkFileDialog
from flask import Flask, request
from flask_restful import Resource, Api
from flask_cors import CORS
app = Flask(__name__)
CORS(app)
api=Api(app)
import webbrowser
import spellCheckPack as sp
q=1
webbrowser.open('file://' + os.path.realpath('index.html'))


class Director(Resource):
    def get(self):
        root = Tkinter.Tk()
        root.withdraw()
        root.wm_attributes('-topmost', 1)
        dirname = tkFileDialog.askdirectory(parent=root,initialdir="/",title='Please select a directory')
        return {"status":"OK","dirname":dirname}


class Executor(Resource):
    def get(self):
        global q
        fileDir=request.args.get('fileDir')
        db_sql = request.args.get('db_sql')
        db_mongo = request.args.get('db_mongo')
        db_dataframe= []
        counter=1
        if db_sql != "":
            username = request.args.get('username')
            password = request.args.get('password')
            f = file_retreiver_sql.main(username,password,counter)
            result = f.readBLOB()
            db_dataframe += result[0]
            counter = result[1]
        if db_mongo != "":
            f= file_retreiver_mongo.main(counter)
            result = f.readMongo()
            db_dataframe += result[0]
            counter = result[1]
        q=searchRanker.Query(fileDir,db_dataframe,counter)
        return {"status":"OK","message":"Server started"}

class HelloWorld(Resource):
    def get(self):
        query=request.args.get('query')
        print(query)
        indexed_results,cStrng=q.query_processor(query,sp)
        results={}
        results['searchResults']={}
        results['correctedQueryString']=cStrng
        count=0
        for val in indexed_results:
            results['searchResults'][count]=(q.index.dataFrame['title'][val-1],q.index.dataFrame['link'][val-1],q.index.dataFrame['content'][val-1][0:200],q.index.dataFrame['tags'][val-1],q.index.dataFrame['summary'][val-1])
            count=count+1
        return results

    
class AddTag(Resource):
    def get(self):
        title = request.args.get('title')
        tags = request.args.get('tags')
        a = tag_adder.main(title,tags)
        res = a.Add()
        return res


class FileOpener(Resource):
    def get(self):
        filepath=request.args.get('pathToFile')
        if(filepath!='sql'):
            try:
                if platform.system() == 'Darwin':       # macOS
                    subprocess.call(('open', filepath))
                elif platform.system() == 'Windows':    # Windows
                    os.startfile(filepath)
                else:                                   # linux variants
                    subprocess.call(('xdg-open', filepath))
            except:
                return {"status":"File Not Found"}
        return {"status":"OK"}
        

class FileAdder(Resource):
    def get(self):
        database = request.args.get('database')
        ftype = request.args.get('ftype')
        title = request.args.get('title')
        file_link = request.args.get('link')
        if database == "SQL":
            username = request.args.get('username')
            password = request.args.get('password')
            f = file_adder_sql.main(username,password,ftype,title,file_link)
            result = f.insertBLOB()
            return result
        if database == "MONGO":
            f= file_adder_mongo.main(title,ftype,file_link)
            result = f.insert()
            return result

class TagDict(Resource):
    def get(self):
        d = tag_dict.main()
        tagDict = d.get_dict()
        result= [list(col) for col in zip(*[d.values() for d in tagDict])]
        return result

class TagSearcher(Resource):
    def get(self):
        tags = request.args.get('tags')
        t = tag_search.main(tags)
        data = t.search()
        return data


api.add_resource(Director, '/directory_select')
api.add_resource(Executor, '/execute')
api.add_resource(HelloWorld, '/get_results')
api.add_resource(AddTag,'/tag_adder')
api.add_resource(TagDict,'/tag_dict')
api.add_resource(FileOpener,'/open_file')
api.add_resource(FileAdder,'/file_adder')
api.add_resource(TagSearcher,'/tag_search')
app.run(debug=False)
