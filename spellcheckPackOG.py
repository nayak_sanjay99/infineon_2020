import re
import pandas as pd
import spacy
import pickle
nlp = spacy.load('en_core_web_sm')


""" this small piece of code takes unique words from our csv content and
    adds it to our dictionary (words like iPhone, iPad, XBox)
"""
# lister=set()
# content=df['content']
# for string in content:
#     temp=string.split()
#     for x in temp:
#         lister.add(x)
        
        
# tokenizing the words using spacy
def tokenize(text):
    return [token.text for token in nlp.tokenizer(text)]


    
""" calculates the Damerau-Levenshtein ditance between a selected valid word (from dictionary) and an user-entered word
    damerau-levenshtein distance examples
        'bee' and 'see' have a D-L distance of 1
        'hat' and 'hats' have a D-L distance of 1
        'brik' and 'blink' have a D-L distance of 2
"""
def damerau_levenshtein_dist(seq1, seq2):
    
    oneago = None
    thisrow = list(range(1, len(seq2) + 1)) + [0]
    for x in range(len(seq1)):
        twoago, oneago, thisrow = (oneago, thisrow, [0] * len(seq2) + [x + 1])  #moving one character ahead
        for y in range(len(seq2)):
            delcost = oneago[y] + 1                                     #calculating deletion cost
            #print("delcost "+str(delcost))
            addcost = thisrow[y - 1] + 1                                #calculating addition cost
            #print("addcost "+str(addcost))
            subcost = oneago[y - 1] + (seq1[x] != seq2[y])              #calculating subtraction cost
            #print("subcost "+str(subcost))
            thisrow[y] = min(delcost, addcost, subcost)
            # The below block deals with transpositions (exchanges)
            if (x > 0 and y > 0 and seq1[x] == seq2[y - 1]
                    and seq1[x - 1] == seq2[y] and seq1[x] != seq2[y]):
                thisrow[y] = min(thisrow[y], twoago[y - 2] + 1)
    #print("returning "+thisrow[len(seq2)-1])
    return thisrow[len(seq2) - 1]



class SpellCorr:
    
    
    def __init__(self, max_edit_distance=3):
        self.max_edit_distance = max_edit_distance
        self.dictionary = {}
        self.longest_word_length = 0


    #return strings with up to max_edit_distance characters deleted
    def get_deletes_list(self, w):

        deletes = []    
        """the above variable will hold words with both 1 deletion and 2 deletions ( this will be returned )
        """
        
        queue = [w]         
        """ the above queue variable will hold words with zero deletes before the first iteration
            and after the first iteration, will hold words with one deletion"""        
        
        for d in range(self.max_edit_distance):
            temp_queue = []
            for word in queue:
                if len(word) > 1:
                    for c in range(len(word)):                          # character index ( to be deleted )
                        word_minus_c = word[:c] + word[c + 1:]          #character at index c is deleted
                        if word_minus_c not in deletes:
                            deletes.append(word_minus_c)
                        if word_minus_c not in temp_queue:
                            temp_queue.append(word_minus_c)
            queue = temp_queue

        return deletes


    """ adds words and its derived deletions to dictionary 
        first we check if the word is already present in the dictionary
        dictionary entries are of the form (list of suggested corr. , freq. in corpus)
    """
    def create_dictionary_entry(self, w):
        new_real_word_added = False
        if w in self.dictionary:                                                    # increment count of word in corpus
            self.dictionary[w] = (self.dictionary[w][0], self.dictionary[w][1] + 1)
        else:                                                                       #creation of new entry
            self.dictionary[w] = ([], 1)
            self.longest_word_length = max(self.longest_word_length, len(w))

        if self.dictionary[w][1] == 1:                                              #if its the first appearance of word in corpus
            new_real_word_added = True
            deletes = self.get_deletes_list(w)
            for item in deletes:
                if item in self.dictionary:
                    self.dictionary[item][0].append(w)                         # add correct word to delete's suggestion list
                else:
                    self.dictionary[item] = ([w], 0)                           # creating a new entry with frequency as 0 ( because it is not present in the dictionary)

        return new_real_word_added


    """ Given an array, this function returns a dictionary created from that array
        The statistical output ( total words processed, longest word ) displaying is done (will be removed after presentation)
    """
    def create_dictionary_from_arr(self, arr, token_pattern=r'[a-z]+'):
        total_word_count = 0
        unique_word_count = 0

        for line in arr:
            words = re.findall(token_pattern, line.lower())
            for word in words:
                total_word_count += 1
                if self.create_dictionary_entry(word):      #checking if a word was not found previously
                    unique_word_count += 1

        print("total words processed: %i" % total_word_count)
        print("total unique words in corpus: %i" % unique_word_count)
        print("total items in dictionary (corpus words and deletions): %i" % len(self.dictionary))
        print("  edit distance for deletions: %i" % self.max_edit_distance)
        print("  length of longest word in corpus: %i" % self.longest_word_length)
        return self.dictionary,self.longest_word_length



    """ returns the best suggested correction for incorrectly spelled word
        by looking up for words in dictionary with minimum edit distance and high frequency
    """
    def get_suggestions(self, string, silent=False):
        
        """ we have assumed our max_edit_distance as 2
            now to understand the below piece of code 
            assume that the user-entered word is of length 12 
            and the longest_word(available) is of length 9
            Now, we cannot suggest any word as even if we try to edit the user-entered word, 
            the maximum number of deletions possible is 2 (max_edit_distance), but we dont have any word of length 10 (12-2)
            since the longest word is of length 9
        """
        if (len(string) - self.longest_word_length) > self.max_edit_distance:
            if not silent:
                print("no items in dictionary within maximum edit distance")
            return []

        suggest_dict = {}
        min_suggest_len = float('inf')

        queue = [string]
        q_dictionary = {}  

        while len(queue) > 0:
            q_item = queue[0]   #peep
            queue = queue[1:]   #pop

            #early exit
            if ((len(suggest_dict) > 0) and ((len(string) - len(q_item)) > min_suggest_len)):
                break

            if (q_item in self.dictionary) and (q_item not in suggest_dict):
                if self.dictionary[q_item][1] > 0:
                    """ word is in dictionary, and is a word from the corpus,but is not present in suggestion list,
                        so add to suggest_dict, format: (freq. in corpus, edit distance)
                    """
                    assert len(string) >= len(q_item)
                    suggest_dict[q_item] = (self.dictionary[q_item][1],
                                            len(string) - len(q_item))
                    
                    """ early exit ( because we have already found the word with distance 0, the best possible possible suggestion) """
                    if len(string) == len(q_item):
                        break
                    elif (len(string) - len(q_item)) < min_suggest_len:
                        """ this is to ensure that the next suggestions aren't at a greater distance than the current suggestion
                            to understand this more clearly, assume that we got a suggestion at distance 1, 
                            so we need not search for suggestions at greater distances, hence we set min_suggest_len variable to 
                            the corresponding value
                        """
                        min_suggest_len = len(string) - len(q_item)

                """ looping through the suggested corrections for q_item as stored in the dctionary """
                for sc_item in self.dictionary[q_item][0]:
                    if sc_item not in suggest_dict:

                        assert len(sc_item) > len(q_item)

                        assert len(q_item) <= len(string)

                        if len(q_item) == len(string):
                            assert q_item == string
                            item_dist = len(sc_item) - len(q_item)

                        assert sc_item != string

                        # calculating the edit distance for the current suggestion
                        item_dist = damerau_levenshtein_dist(sc_item, string)

                        
                        if item_dist > min_suggest_len:
                            """ we reject the suggestion if its distance is greater than min_suggest_len 
                            ( because we already have better suggestions of edit_distance <= min_suggest_len )
                            """
                            pass
                        elif item_dist <= self.max_edit_distance:
                            assert sc_item in self.dictionary  # should already be in dictionary if in suggestion list
                            suggest_dict[sc_item] = (self.dictionary[sc_item][1], item_dist)    # add to suggestions dictionary
                            if item_dist < min_suggest_len:                 #updating the min_suggest_len, if necessary
                                min_suggest_len = item_dist

                        """keeping only those items whose distance is less than or equal to the latest updated min_suggest_len"""
                        suggest_dict = {k: v for k, v in suggest_dict.items() if v[1] <= min_suggest_len}

            assert len(string) >= len(q_item)

            if (len(string) - len(q_item)) > min_suggest_len:
                pass
            elif (len(string) - len(q_item)) < self.max_edit_distance and len(q_item) > 1:
                for c in range(len(q_item)):  # character index
                    word_minus_c = q_item[:c] + q_item[c + 1:]              #removing the c-indexed character
                    if word_minus_c not in q_dictionary:
                        queue.append(word_minus_c)
                        q_dictionary[word_minus_c] = None  # arbitrary value, just to identify we checked this

        as_list = suggest_dict.items()
        """sorting the suggested items based on D-L distance"""
        outlist = sorted(as_list, key=lambda x: (x[1][1], -x[1][0]))

        return outlist[0]       # returning only the best suggestion



    """ this function is calls get_suggestions function 
        Then why have we written this function??
        because, if there are no correct words within max_edit_distance range, then 
        get_suggestions function would return nothing,
        To handle this case, this function is used
    """
    def best_word(self, s):
        try:
            return self.get_suggestions(s, False)[0]
        except:
            return s             # if nothing is returned by get_suggestions function



""" words_d is a list of correctly spelled words 
    word_list is the list of words created by space splitting the user query
    This function checks whether each word from query is present in the lookup dictionary(words_d)
    if it is present, that means the word is correct and 
    if it is not, that means the word is incorrect and hence, the best_word function is called.
"""
def spell_corrector(word_list, words_d) -> str:
    result_list = []
    for word in word_list:
        if word not in words_d:                             # this condition becoming true means incorrectly spelled word
            suggestion = ss.best_word(word)    # the most probable corrected word is returned
            if suggestion is not None:
                result_list.append(suggestion)              
        else:                                               # entered word is correct and hence move on
            result_list.append(word)
            
    return " ".join(result_list)



ss = SpellCorr(max_edit_distance=2)

with open('./words_alpha.txt') as f:                                            #english_words_479k
    words = f.readlines()
eng_words = [word.strip() for word in words]

print('Total english words: {}'.format(len(eng_words)))

print('creating SpellCorr dictionary...')

all_words_list = list(set(eng_words))      
silece,longes = ss.create_dictionary_from_arr(all_words_list, token_pattern=r'.+')


""" creating a dictionary of correctly spelled words for lookup """
words_dict = {k: 0 for k in all_words_list}

saver={"dictionary":silece,"longest_word_length":longes,"words_dict":words_dict}
with open('dictionary.pickle', 'wb') as handle:
    pickle.dump(saver, handle, protocol=pickle.HIGHEST_PROTOCOL)

# Load data (deserialize)


""" Given an input string, this functions returns the corrected string
    spacy tokenizer is called which returns a list of words following which the spell_corrector function is called 
    which has the base logic for spelling correction.
"""
def inOut(input_string):
    sample_text = input_string
    tokens = tokenize(sample_text)
    corrected_text = spell_corrector(tokens, words_dict)
    return corrected_text 