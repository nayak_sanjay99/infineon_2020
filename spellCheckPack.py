import pickle
import spacy
nlp=spacy.load('en_core_web_sm')


def tokenize(text):
    return [token.text for token in nlp.tokenizer(text)]


with open('dictionary.pickle', 'rb') as handle:
    saver = pickle.load(handle)
print(saver.keys())
longest_word_length=saver['longest_word_length']
words_dict=saver['words_dict']
dictionary=saver['dictionary']
max_edit_distance=2

def get_suggestions(string, silent=False):
    
    """ we have assumed our max_edit_distance as 2
        now to understand the below piece of code 
        assume that the user-entered word is of length 12 
        and the longest_word(available) is of length 9
        Now, we cannot suggest any word as even if we try to edit the user-entered word, 
        the maximum number of deletions possible is 2 (max_edit_distance), but we dont have any word of length 10 (12-2)
        since the longest word is of length 9
    """
    if (len(string) - longest_word_length) > max_edit_distance:
        if not silent:
            print("no items in dictionary within maximum edit distance")
        return []

    suggest_dict = {}
    min_suggest_len = float('inf')

    queue = [string]
    q_dictionary = {}  

    while len(queue) > 0:
        q_item = queue[0]   #peep
        queue = queue[1:]   #pop

        #early exit
        if ((len(suggest_dict) > 0) and ((len(string) - len(q_item)) > min_suggest_len)):
            break

        if (q_item in dictionary) and (q_item not in suggest_dict):
            if dictionary[q_item][1] > 0:
                """ word is in dictionary, and is a word from the corpus,but is not present in suggestion list,
                    so add to suggest_dict, format: (freq. in corpus, edit distance)
                """
                assert len(string) >= len(q_item)
                suggest_dict[q_item] = (dictionary[q_item][1],
                                        len(string) - len(q_item))
                
                """ early exit ( because we have already found the word with distance 0, the best possible possible suggestion) """
                if len(string) == len(q_item):
                    break
                elif (len(string) - len(q_item)) < min_suggest_len:
                    """ this is to ensure that the next suggestions aren't at a greater distance than the current suggestion
                        to understand this more clearly, assume that we got a suggestion at distance 1, 
                        so we need not search for suggestions at greater distances, hence we set min_suggest_len variable to 
                        the corresponding value
                    """
                    min_suggest_len = len(string) - len(q_item)

            """ looping through the suggested corrections for q_item as stored in the dctionary """
            for sc_item in dictionary[q_item][0]:
                if sc_item not in suggest_dict:

                    assert len(sc_item) > len(q_item)

                    assert len(q_item) <= len(string)

                    if len(q_item) == len(string):
                        assert q_item == string
                        item_dist = len(sc_item) - len(q_item)

                    assert sc_item != string

                    # calculating the edit distance for the current suggestion
                    item_dist = damerau_levenshtein_dist(sc_item, string)

                    
                    if item_dist > min_suggest_len:
                        """ we reject the suggestion if its distance is greater than min_suggest_len 
                        ( because we already have better suggestions of edit_distance <= min_suggest_len )
                        """
                        pass
                    elif item_dist <= max_edit_distance:
                        assert sc_item in dictionary  # should already be in dictionary if in suggestion list
                        suggest_dict[sc_item] = (dictionary[sc_item][1], item_dist)    # add to suggestions dictionary
                        if item_dist < min_suggest_len:                 #updating the min_suggest_len, if necessary
                            min_suggest_len = item_dist

                    """keeping only those items whose distance is less than or equal to the latest updated min_suggest_len"""
                    suggest_dict = {k: v for k, v in suggest_dict.items() if v[1] <= min_suggest_len}

        assert len(string) >= len(q_item)

        if (len(string) - len(q_item)) > min_suggest_len:
            pass
        elif (len(string) - len(q_item)) < max_edit_distance and len(q_item) > 1:
            for c in range(len(q_item)):  # character index
                word_minus_c = q_item[:c] + q_item[c + 1:]              #removing the c-indexed character
                if word_minus_c not in q_dictionary:
                    queue.append(word_minus_c)
                    q_dictionary[word_minus_c] = None  # arbitrary value, just to identify we checked this

    as_list = suggest_dict.items()
    """sorting the suggested items based on D-L distance"""
    outlist = sorted(as_list, key=lambda x: (x[1][1], -x[1][0]))

    return outlist[0]       # returning only the best suggestion

def best_word(s):
    try:
        return get_suggestions(s, False)[0]
    except:
        return s

def spell_corrector(word_list, words_d) -> str:
    result_list = []
    for word in word_list:
        if word not in words_d:                             # this condition becoming true means incorrectly spelled word
            suggestion = best_word(word)    # the most probable corrected word is returned
            if suggestion is not None:
                result_list.append(suggestion)              
        else:                                               # entered word is correct and hence move on
            result_list.append(word)
            
    return " ".join(result_list)


def inOut(input_string):
    sample_text = input_string
    tokens = tokenize(sample_text)
    corrected_text = spell_corrector(tokens, words_dict)
    return corrected_text

print(inOut('helw'))