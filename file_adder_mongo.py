from pymongo import MongoClient as Connection
from gridfs import GridFS

class FileAdderMongo:
    def __init__(self,title,ftype,link):
        self.db = Connection().myfiles
        self.fs = GridFS(self.db)
        self.title = title
        self.link = link
        self.ftype = ftype
    def insert(self):
        with open(self.link,'rb') as file:
            content_type = "document/"+self.ftype
            self.fs.put(file,content_type=content_type,filename=self.title)
        return {"result":"File inserted successfully."}

def main(title,ftype,link):
    f = FileAdderMongo(title,ftype,link)
    return f 