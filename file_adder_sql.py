import mysql.connector 
from mysql.connector import Error

class FileAdderSql:
    def __init__(self,username,password,ftype, title, link):
        self.ftype = ftype
        self.title = title
        self.link = link
        self.username = username
        self.password = password

    def insertBLOB(self):
        try:
            connection = mysql.connector.connect(host='localhost',
                                                database='myfiles',
                                                user=self.username,
                                                password=self.password)

            cur = connection.cursor()
            sql_insert_blob_query = """ INSERT INTO files
                            (ftype, title, content) VALUES (%s,%s,%s)"""

            with open(self.link,'rb') as file:
                bin_content = file.read()

            content =  bin_content.encode('UTF-8')
            insert_blob_tuple = (self.ftype, self.title, content)
            result = cur.execute(sql_insert_blob_query, insert_blob_tuple)
            connection.commit()
            if (connection.is_connected()):
                cur.close()
                connection.close()
                print("MySQL connection is closed")
            return {"result":"File inserted successfully."}
        except mysql.connector.Error as error:
            print("Failed {}".format(error))

def main(username,password,ftype,title,link):
    f = FileAdderSql(username,password,ftype,title,link)
    return f

