import pandas as pd
import pickle
import os

class TagSearcher:
    def __init__(self,tags):
        self.tags=list(tags.split(","))
        self.pickleData=pd.read_pickle('mypickle.pickle')

    def search(self):
        tag_pickle=list(self.pickleData['tags'])
        title_pickle=list(self.pickleData['title'])
        tag_data = []
        for tag_str in tag_pickle:
            tags = list(tag_str.split(",")) 
            tag_data.append(tags)
        dtt = {}
        tsize = len(title_pickle)
        for i in range(0,tsize):
            for tag in tag_data[i]:
                dtt[tag] = title_pickle[i]
        title_dict={}
        for tag in self.tags:
            filename=dtt.get(tag)
            if title_dict.get(filename) == None:
                title_dict[filename] = 1
            else:
                val =  title_dict[filename]
                val += 1
                title_dict[filename] = val
        title_dict = {k: v for k, v in sorted(title_dict.items(), key=lambda item: item[1],reverse=True)}
        print(title_dict)
        data = {"searchResults":[]}
        for filename in title_dict:
            temp = []
            temp.append(filename)
            link = self.pickleData[self.pickleData["title"]==filename]["link"]
            link = link.tolist()
            temp.append(link[0])
            content =  self.pickleData[self.pickleData["title"]==filename]["content"]
            content = content.tolist()
            cont=content[0]
            temp.append(cont[0:200])
            associated_tags =  self.pickleData[self.pickleData["title"]==filename]["tags"]
            associated_tags = associated_tags.tolist()
            temp.append(associated_tags[0])
            associated_summary =  self.pickleData[self.pickleData["title"]==filename]["summary"]
            associated_summary = associated_summary.tolist()
            temp.append(associated_summary[0])
            data["searchResults"].append(temp)
        return data

def main(tags):
    t = TagSearcher(tags)
    return t